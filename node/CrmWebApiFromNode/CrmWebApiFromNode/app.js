﻿/// Require external libraries
var adal = require("adal-node");
var https = require("https");
var Q = require("q");                       /// Asynchronous development; get out of call-back hell
var odatajs = require("odatajs");
var netHttps = require("./net-https.js");   /// Because node.js doesn't make any assumptions for you

/// This is necessary to leverage Olingo libraries with CRM, which requires the Content-Length header
odatajs.oData.net.defaultHttpClient = netHttps.defaultHttpClient;

/// Put together all of the necessary components for authentication via ADAL
var authorityHostUrl = "https://login.windows.net";
var tenant = "f2a51796-fe73-4de5-992e-a6ba18f8551c";
var clientId = "080afc9b-68bc-4b5b-ab3e-dc9110a7918f";
var username = "david@sntdn.onmicrosoft.com";
var password = "pass@word1";
var resource = "Microsoft.CRM";             /// This is for multi-tenant; Guid of the resource for specific

var authorityUrl = authorityHostUrl + "/" + tenant;

/// Declare our CRM service endpoint
var crmWebApiEndpoint = "https://sntdn.crm6.dynamics.com/api/data";

/// Initialize ADAL
var adalContext = new adal.AuthenticationContext(authorityUrl);

/// Shell variables
var bearerToken  = null;

/// Dynamic header compilation
var getCrmServiceHeaders = function () {
    return {
        "Authorization": "Bearer " + bearerToken,
        "Accept": "application/json",
        "Content-Type": "application/json; charset=utf-8",
        "OData-MaxVersion": "4.0",
        "OData-Version": "4.0"
    };
}

/// Create a deferral object for async program modeling
var authentication = Q.defer();

authentication.promise
    /// When authentication is complete, retrieve all Cases (incidents)
    .then(
    function (accessToken) {
        bearerToken = accessToken;
        
        /// Setup a promise for the incident transaction
        var incidentRetrieve = Q.defer();
        
        /// The request for our incidents
        var incidentRequest = {
            requestUri: crmWebApiEndpoint + "/incidents?$select=title",
            method: "GET",
            headers: getCrmServiceHeaders()
        };
        
        /// Execute the request for incidents
        odatajs.oData.request(incidentRequest,
            function (data, response) {
                /// We have our incidents
                incidentRetrieve.resolve(data.value);
                console.log(data.value);
            },
            function (data, response) {
                incidentRetrieve.reject(response);
            });
        
        /// Chain our Incident retrieval promise
        return incidentRetrieve.promise;
    })
    .done(
        /// The incident transaction promise is complete!
        function (incidents) {
            debugger;
        });

/// Authenticate with Azure
adalContext.acquireTokenWithUsernamePassword(
    resource,
    username,
    password,
    clientId,
    function (err, tokenResponse) {
        if (err) {
            console.log('well that didn\'t work: ' + err.stack);

            authentication.reject(err.stack);
        } else {
            console.log(tokenResponse);
            
            /// Fulfill the authentication promise
            authentication.resolve(tokenResponse.accessToken);
        }
    });
