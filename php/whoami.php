<?php 
require_once "./php-crm-sdk/AlexaSDK_Abstract.php";
require_once "./php-crm-sdk/config.php";
require_once "./oauth.php";



if ($access_token){
	/* WHOAMI REQUEST USING BEARER TOKEN */
	$url = $settings->organizationUrl."/web";

	$scheme = parse_url($url);

	$request = "<s:Envelope xmlns:s='http://schemas.xmlsoap.org/soap/envelope/'>
					<s:Body>
					<Execute xmlns='http://schemas.microsoft.com/xrm/2011/Contracts/Services' xmlns:i='http://www.w3.org/2001/XMLSchema-instance'>
						<request i:type='b:WhoAmIRequest' xmlns:a='http://schemas.microsoft.com/xrm/2011/Contracts' xmlns:b='http://schemas.microsoft.com/crm/2011/Contracts'>
						<a:Parameters xmlns:c='http://schemas.datacontract.org/2004/07/System.Collections.Generic' />
						<a:RequestId i:nil='true' />
						<a:RequestName>WhoAmI</a:RequestName>
						</request>
					</Execute>
					</s:Body>
				</s:Envelope>";

	$headers = array(     
		"POST ". $scheme["path"]." HTTP/1.1",
		"Host: ".$scheme["host"],
		"Authorization: Bearer ".$access_token,
		"Content-Type: text/xml; charset=utf-8",
		"SOAPAction: http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute",
		"Content-length: " . strlen($request)
	);

	$cURLHandle = curl_init();
	curl_setopt($cURLHandle, CURLOPT_URL, $url);
	curl_setopt($cURLHandle, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($cURLHandle, CURLOPT_TIMEOUT, 60);
	curl_setopt($cURLHandle, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($cURLHandle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	curl_setopt($cURLHandle, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($cURLHandle, CURLOPT_POST, 1);
	curl_setopt($cURLHandle, CURLOPT_POSTFIELDS, $request);
	curl_setopt($cURLHandle, CURLINFO_HEADER_OUT, $request);

	$response = curl_exec($cURLHandle);

	$curlErrno = curl_errno($cURLHandle);

	if($curlErrno){
		$curlError = curl_error($cURLHandle);
		throw new Exception($curlError);
	}

	$content_type = curl_getinfo($cURLHandle);

	if ($content_type["http_code"] == 200){
		/* SUSTEMUSER FULL NAME REQUEST USING BEARER TOKEN */
		$soap = new DomDocument();
		$soap->loadXML($response);

		$userid = $soap->getElementsbyTagName("value")->item(0)->textContent;
	}else{
		$userid = NULL;
	}
}
?>
<?php include("header.php"); ?>


<!-- Page Title -->
<div class="section section-breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Who Am I</h1>
			</div>
		</div>
	</div>
</div>
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				SystemuserID: <?php echo $userid; ?>
			</div>
		</div>
	</div>
</div>
	
<?php include("footer.php"); ?>