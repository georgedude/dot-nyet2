<?php include("header.php"); ?>

<?php 
require_once "./php-crm-sdk/AlexaSDK_Abstract.php";
require_once "./php-crm-sdk/config.php";

$session = (isset($_COOKIE["login_session"])) ? json_decode($_COOKIE["login_session"]) : null;

if ($session){
	
	$client = new AlexaSDK($settings);
	
	$fetch = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">
				<entity name="savedquery">
				  <attribute name="name" />
				  <attribute name="fetchxml" />
				  <filter type="and">
					<condition attribute="name" operator="eq" value="Customer Invoices" />
				  </filter>
				</entity>
			  </fetch>';

	$view = $client->retrieveSingle($fetch);
	
	
	$query = new SimpleXMLElement($view->fetchxml);
	$condition = $query->xpath('.//condition[@uitype]'); 

	if (count($condition) > 0){
		
		$newCondition = $condition[0];
		$newCondition["uiname"] = $session->EntityName;
		$newCondition["value"] = $session->RecordId;

		$fetch = str_replace($condition[0]->asXML(),$newCondition->asXML(),$query->asXML());
	}	
	
	$invoices = $client->retrieveMultiple($fetch);
}
?>

<!-- Page Title -->
		<div class="section section-breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Invoices</h1>
					</div>
				</div>
			</div>
		</div>
        <div class="section">
	    	<div class="container">
	    		<div class="row">
	    			<!-- Open Vacancies List -->
	    			<div class="col-md-12">
	    				<table class="jobs-list">
	    					<tr>
	    						<th>Name</th>
	    						<th>Customer</th>
	    						<th>Status Reason</th>
								<th>Total Amount</th>
	    					</tr>
							<?php foreach($invoices->Entities as $invoice) : ?>
									<tr>
										<!-- Position -->
										<td class="job-position">
											<?php echo $invoice->displayname; ?>
										</td>
										<!-- Location -->
										<td class="job-location">
											<?php echo $invoice->getFormattedValue("customerid"); ?>
										</td>
										<!-- Job Type -->
										<td class="job-type hidden-phone"><?php echo $invoice->getFormattedValue("statuscode"); ?></td>
										<td class="job-type hidden-phone"><?php echo $invoice->getFormattedValue("totalamount"); ?></td>
									</tr>
							<?php endforeach; ?>
	    				</table>
	    			</div>
	    			<!-- End Open Vacancies List -->
	    		</div>
			</div>
		</div>

<?php include("footer.php"); ?>