<?php
$sessionOauth = (isset($_COOKIE["bearer"])) ? json_decode($_COOKIE["bearer"]) : null;
	
/* GET RESPONSE */
if (isset($_GET["code"]) && isset($_GET["session_state"])){
	
		unset($_COOKIE['bearer']);
		setcookie('bearer', null, -1, '/');
		$sessionOauth = (isset($_COOKIE["bearer"])) ? json_decode($_COOKIE["bearer"]) : null;
	
		$code = $_GET["code"];
	
		$RedirectUri = "http" . (($_SERVER['SERVER_PORT']==443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		
		$purl = parse_url($RedirectUri);
		
		$RedirectUri = $purl["scheme"]."://".$purl["host"].$purl["path"];
		
		$requestBody = "grant_type=authorization_code&code=".$code."&client_id=".$oauth["clientID"]."&redirect_uri=".urlencode($RedirectUri)."&resource=".$oauth["resource"]."&client_secret=".$oauth["clientSecret"];
		
		$url = str_replace("?api-version=1.0", "",  $oauth["tokenEndpoint"]);
		
		$scheme = parse_url($url);
            
            $headers = array(     
                "POST ".$scheme["path"]." HTTP/1.1",
                "Host: ".$scheme["host"],
                "Content-type: application/x-www-form-urlencoded;charset=UTF-8",
                "Content-length: " . strlen($requestBody),
            );
            
            $cURLHandle = curl_init();
            curl_setopt($cURLHandle, CURLOPT_URL, $url);
            curl_setopt($cURLHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($cURLHandle, CURLOPT_TIMEOUT, 60);
            curl_setopt($cURLHandle, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($cURLHandle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($cURLHandle, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($cURLHandle, CURLOPT_POST, 1);
            curl_setopt($cURLHandle, CURLOPT_POSTFIELDS, $requestBody);

            $curlErrno = curl_errno($cURLHandle);
            if($curlErrno){
                $curlError = curl_error($cURLHandle);
                throw new Exception($curlError);
            }

            $response = curl_exec($cURLHandle);
			
            $resp = json_decode($response, true );
            
            $cookie["access_token"] = $resp["access_token"];
            $cookie["refresh_token"] = $resp["refresh_token"];
            $cookie["expires_on"] = $resp["expires_on"];
            $cookie["expires_in"] = $resp["expires_in"];
			
			setcookie(  "bearer", 
					json_encode($cookie), 
					(time() + 2 * 86400));
			
			$sessionOauth = (isset($_COOKIE["bearer"])) ? json_decode($_COOKIE["bearer"]) : null;
            
            $access_token = $resp["access_token"];
			
}else if (isset($_GET["error"])){
		
}else{
	
	if ($sessionOauth && $sessionOauth->access_token && ($sessionOauth->expires_on > time())){

		$access_token = $sessionOauth->access_token;
	}else{

		unset($_COOKIE['bearer']);
		setcookie('bearer', null, -1, '/');

		$state = "943e2a91-a3f4-49a2-964b-0ccaffef82d3";
		
		$RedirectUri = "http" . (($_SERVER['SERVER_PORT']==443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']."?";
		
		$purl = parse_url($RedirectUri);
		
		$RedirectUri = $purl["scheme"]."://".$purl["host"].$purl["path"];
		
		$url = $oauth["authorizationEndpoint"]."?response_type=code&client_id=".$oauth["clientID"]."&resource=".$oauth["resource"]."&redirect_uri=".urlencode($RedirectUri)."&state=".$state;

		?>
		<script>
			window.location.href = "<?php echo $url; ?>";
		</script>
		<?php 
	}
}