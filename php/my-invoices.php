<?php include("header.php"); ?>

<?php

require_once "./php-crm-sdk/AlexaSDK_Abstract.php";
require_once "./php-crm-sdk/config.php";
require_once "./oauth.php";

if ($access_token) {

	$client = new AlexaSDK($settings);

	$fetch = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">
                            <entity name="savedquery">
                              <all-attributes />
                              <filter type="and">
                                <condition attribute="name" operator="eq" value="My Invoices" />
                              </filter>
                            </entity>
                          </fetch>';

	$view = $client->retrieveSingle($fetch);
	
	$fetchXML = htmlspecialchars($view->fetchxml);

	$url = $settings->organizationUrl . "/web";

	$scheme = parse_url($url);

	$request = '
		<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
		  <s:Body>
			<RetrieveMultiple xmlns="http://schemas.microsoft.com/xrm/2011/Contracts/Services" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
			  <query i:type="a:FetchExpression" xmlns:a="http://schemas.microsoft.com/xrm/2011/Contracts">
				<a:Query>'.$fetchXML.'</a:Query>
			  </query>
			</RetrieveMultiple>
		  </s:Body>
		</s:Envelope>';
	
	$headers = array(
		"POST " . $scheme["path"] . " HTTP/1.1",
		"Host: " . $scheme["host"],
		"Authorization: Bearer " . $access_token,
		"Content-Type: text/xml; charset=utf-8",
		"SOAPAction: http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/RetrieveMultiple",
		"Content-length: " . strlen($request)
	);
	
	$cURLHandle = curl_init();
	curl_setopt($cURLHandle, CURLOPT_URL, $url);
	curl_setopt($cURLHandle, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($cURLHandle, CURLOPT_TIMEOUT, 60);
	curl_setopt($cURLHandle, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($cURLHandle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	curl_setopt($cURLHandle, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($cURLHandle, CURLOPT_POST, 1);
	curl_setopt($cURLHandle, CURLOPT_POSTFIELDS, $request);

	$response = curl_exec($cURLHandle);
	
	$content_type = curl_getinfo($cURLHandle);

	$curlErrno = curl_errno($cURLHandle);
	if ($curlErrno) {
		$curlError = curl_error($cURLHandle);
		throw new Exception($curlError);
	}

	$invoices = $client->parseRetrieveMultipleResponse($client, $response, false);
}
?>

		<!-- Page Title -->
		<div class="section section-breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Invoices</h1>
					</div>
				</div>
			</div>
		</div>
        <div class="section">
	    	<div class="container">
	    		<div class="row">
	    			<!-- Open Vacancies List -->
	    			<div class="col-md-12">
	    				<table class="jobs-list">
	    					<tr>
	    						<th>Name</th>
	    						<th>Customer</th>
	    						<th>Status Reason</th>
								<th>Total Amount</th>
	    					</tr>
							<?php foreach($invoices->Entities as $invoice) : ?>
									<tr>
										<!-- Position -->
										<td class="job-position">
											<?php echo $invoice->displayname; ?>
										</td>
										<!-- Location -->
										<td class="job-location">
											<?php echo $invoice->getFormattedValue("customerid"); ?>
										</td>
										<!-- Job Type -->
										<td class="job-type hidden-phone"><?php echo $invoice->getFormattedValue("statuscode"); ?></td>
										<td class="job-type hidden-phone"><?php echo $invoice->getFormattedValue("totalamount"); ?></td>
									</tr>
							<?php endforeach; ?>
	    				</table>
	    			</div>
	    			<!-- End Open Vacancies List -->
	    		</div>
			</div>
		</div>



<?php include("footer.php"); ?>